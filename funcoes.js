
;(function($,window){var get_win_size=function(){if(window.innerWidth!=undefined)return[window.innerWidth,window.innerHeight];else{var B=document.body;var D=document.documentElement;return[Math.max(D.clientWidth,B.clientWidth),Math.max(D.clientHeight,B.clientHeight)]}};$.fn.center=function(opt){var $w=$(window);var scrollTop=$w.scrollTop();return this.each(function(){var $this=$(this);var configs=$.extend({against:"window",top:false,topPercentage:0.5,resize:true},opt);var centerize=function(){var against=configs.against;var against_w_n_h;var $against;if(against==="window")against_w_n_h=get_win_size();else if(against==="parent"){$against=$this.parent();against_w_n_h=[$against.width(),$against.height()];scrollTop=0}else{$against=$this.parents(against);against_w_n_h=[$against.width(),$against.height()];scrollTop=0}var x=(against_w_n_h[0]-$this.outerWidth())*0.5;var y=(against_w_n_h[1]-$this.outerHeight())*configs.topPercentage+scrollTop;if(configs.top)y=configs.top+scrollTop;$this.css({"left":x,"top":y})};centerize();if(configs.resize===true)$w.resize(centerize)})}})(jQuery,window);

var header = {
	'Accept': 'application/json',
	'REST-range': 'resources=0-10',
	'Content-Type': 'application/json; charset=utf-8'
};

// ldskjdslkfjsldkdsj

var insertMasterData = function(ENT, loja, dados, fn) {
	$.ajax({
		url: '/api/dataentities/' + ENT + '/documents/',
		type: 'PATCH',
		data: dados,
		headers: header,
		success: function(res) {
			fn(res);
		},
		error: function(res) {
		}
	});
};

var selectMasterData = function(ENT, loja, params, fn) {
	// Consulta dados na master data
	$.ajax({
		url: '/api/dataentities/' + ENT + '/search?' + params,
		type: 'GET',
		headers: header,
		success: function(res) {
			fn(res);
		},
		error: function(res) {
		}
	});
};

$(window).on('load', function(){
	if ($('body').hasClass('home')) {
		setTimeout(function(){
			$('.vip-list').addClass('active');
			$('body').append('<div class="modal-overlay"></div>');
		},2000);
	}

    if ($('body').hasClass('categoria departamento')) {
        $('.smarthint').css('display', 'none');
    }

	 $(document).on('click', '.modal-overlay', function(){
	 	$(this).remove();
	 	$('.vip-list').removeClass('active');
	 });

	 $(document).on('click', '.close-vip-list', function(){
	 	$('.modal-overlay').remove();
	 	$('.vip-list').removeClass('active');
	 });

	$('.vip-list form').on("submit", function(e) {
		e.preventDefault();
		// var sexo = $('input[name="genero"]:checked').val();
		// var nome = $('#vip-list-nome').val();
		var email = $('#popup-email').val();
		// var whatsapp = $('#vip-list-whatsapp').val();


		var obj_dados = {
			"email"  : email
		}

		var json_dados = JSON.stringify(obj_dados);
        console.log(obj_dados);

		insertMasterData("CN", 'pierrecardin', json_dados, function(res) {
        	console.log(res);
        	setTimeout(function() {

				jQuery.support.cors = true;
				$.ajax({
				    url: 'https://news.mailclick.me/subscribe.php',
				    type: 'post',
				    data: {
				     "FormValue_Fields[EmailAddress]": email,
				    //  "FormValue_Fields[CustomField5733]": whatsapp,
				    //  "FormValue_Fields[CustomField4410]": nome,
				     "FormValue_ListID": 4632,
				     "FormValue_Command": "Subscriber.Add",
				     "FormButton_Subscribe": "OK"
				    },
				    success: function(res) {
				   		console.log('mlbz ok');
				   		console.log(res);
				    }
				  });

        		$('.radio-news').remove();
				$('.half-column h2').remove();
				$('.half-column h3').remove();
				$('.half-column form p').remove();
				$('.half-column form input').remove();

        		$('.half-column form').append('<div class="full msg_sucesso" style="color:black; margin-top: 50%;">Obrigado por se cadastrar, aproveite o seu cupom de desconto. <br /> <strong style="font-weight: bold; font-size: 20px;">BEMVINDOPIERRE10</strong></div>');
        	}, 1000);
        	setTimeout(function() {
        		// $('.msg_sucesso').remove();
        	}, 10000);
        });
	});

});


/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD (Register as an anonymous module)
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS
		module.exports = factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (arguments.length > 1 && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {},
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling $.cookie().
			cookies = document.cookie ? document.cookie.split('; ') : [],
			i = 0,
			l = cookies.length;

		for (; i < l; i++) {
			var parts = cookies[i].split('='),
				name = decode(parts.shift()),
				cookie = parts.join('=');

			if (key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));


function apenasNumeros(string)
{
    var numsStr = string.replace(/[^0-9]/g,'');
    return parseInt(numsStr);
}
function somaManos()
{

	var elems = $('.full.kit.ativo');
	var arry = $.makeArray(elems)
	var total = 0;
	for (var i = 0; i <= arry.length; i++) {
	    total += $('.full.kit.ativo:eq('+i+')').attr('data-valor') << 0;
	    if(i==arry.length){
	    	var dividido = total/6;
	    	var dividido = dividido.toFixed(0)
	    	$('.kit_final .right .subtotal strong').html(dividido);
			$('.kit_final .right .subtotal strong').priceFormat({centsSeparator:",",thousandsSeparator:"."});
	    	$('.kit_final .right .subtotal strong').prepend('R$ ');
	    	$('.kit_final .right .valor-vista').html(total)
			$('.kit_final .right .valor-vista').priceFormat({centsSeparator:",",thousandsSeparator:"."});
	    	$('.kit_final .right .valor-vista').prepend('Ou ÃƒÂ¯Ã‚Â¿Ã‚Â½ vista por R$  ');
	    }
	}
}

function addCart(event){
	var parente = $(event.target).parents('li')
	var skus_click = parente.find('.ativo').attr('data-sku');
	var seller_click = parente.find('.ativo').attr('data-seller');
	var quantidade = 1;
	// console.log(skus_click);
	// console.log(seller_click);
	// console.log(quantidade);
	item = {
	    id: skus_click,
	    quantity: quantidade,
	    seller: seller_click
	};
	vtexjs.checkout.addToCart([item]).done(function(orderForm){
		// console.log([item]);
	    // console.log(orderForm);
	});
	$(event.target).text('PRODUTO ADICIONADO')
	// $('#carrinhoFloat').css('right', '0');
	setTimeout(function(){
		$(event.target).text('COMPRAR')
		parente.removeClass('adicionado')
		// $('#carrinhoFloat').removeAttr('style')
	},3500);
}

function formatReal(int) {
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if (tmp.length > 6)
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    return tmp;
}

function ameCashBack() {
    $('.ame_cashback').each(function(index, element) {
		var id_prat = $(this).data('id');

		vtexjs.catalog.getProductWithVariations(id_prat).done(function(product) {
			$.each(product.skus, function (index, sku) {
				if (sku.available != false) {
					var preco = sku.bestPrice;
					var percentage = (preco * 30) / 100;
					var percentage_number = (percentage * 100) / preco;
					var money_percent = formatReal(percentage);
					
					// console.log(preco);
					// console.log(percentage);
					// console.log(percentage_number);
					// console.log(money_percent);
					// console.log(element);
					
					$(element).find('span#cash_back').html("R$" + money_percent + "<br>");
                    
				}
			});
		});
	});
    
    if ($('body').hasClass('mobProduto')) {
		$.each(skuJson.skus, function (index, sku){
		    if (sku.available != false) {
				var preco = sku.bestPrice
				var percentage = (preco * 30) / 100;
				var percentage_number = (percentage * 100) / preco;
				var money_percent = formatReal(percentage);
			
				// console.log(preco);
				// console.log(percentage);
				// console.log(percentage_number);
				// console.log(money_percent);
				// console.log(element);
			
				$(".cashback_ame").css("display", "inline-flex");
				$('.cashback_ame .cash_back').html("R$" + money_percent);
		    }
		});
	}
}

$.ajax({
	type: 'GET',
	url: '/api/vtexid/pub/authenticated/user'
	}).done(function (user) {
		console.log(user);
		
		if (user === null) {
			$('#logado').removeClass('actived');
			$('#login').addClass('actived');
		} else {
			$('#logado').addClass('actived');
			$('#login').removeClass('actived');
		}
	});

var carrinhoFlutante = {

	toggle_carrinho: function () {
		$('header .cart a').on('click', function (event) {
			event.preventDefault();
			$('#cart-lateral, #overlay').toggleClass('active');
		});
	},

	calculateShipping: function () {
		if ($('#search-cep input[type="text"]').val() != '') {
			vtexjs.checkout.getOrderForm()
				.then(function (orderForm) {
					if (localStorage.getItem('cep') === null) {
						var postalCode = $('#search-cep input[type="text"]').val();
					} else {
						var postalCode = localStorage.getItem('cep');
					}

					var country = 'BRA';
					var address = {
						"postalCode": postalCode,
						"country": country
					};

					//EFEITO FADE
					$('#cart-lateral .footer ul li span.frete .box-1').fadeOut(300, function () {
						$('#cart-lateral .footer ul li span.frete .box-2').fadeIn(300);
					});

					return vtexjs.checkout.calculateShipping(address);
				})
				.done(function (orderForm) {
					if (orderForm.totalizers.length == 0) {
						$('#cart-lateral .value-frete').text('Você não tem itens no carrinho!');
					} else {
						var value_frete = orderForm.totalizers[1].value / 100;
						value_frete = value_frete.toFixed(2).replace('.', ',').toString();
						$('#cart-lateral .value-frete').text('R$: ' + value_frete);

						var postalCode = $('#search-cep input[type="text"]').val();
						localStorage.setItem('cep', postalCode);
						$('#search-cep input[type="text"]').val(postalCode);
					}
				});
		}
	},

	//APOS INSERIDO - CALCULA FRETE AO CARREGAR A PG
	automaticCalculateShipping: function () {
		$(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
			if (localStorage.getItem('cep') != null) {
				$('#search-cep input[type="text"]').val(localStorage.getItem('cep'));
				carrinhoFlutante.calculateShipping();
			}
		});
	},

	fadeAction: function () {
		$('#cart-lateral .footer ul li span.frete a').on('click', function (e) {
			e.preventDefault();
			$('#cart-lateral .footer ul li span.frete a').fadeOut(300, function () {
				$('#cart-lateral .footer ul li span.frete .box-1 div').fadeIn(300);
			});
		});

		//CALCULAR NOVAMENTE
		$('#cart-lateral .return-frete').on('click', function () {
			$('#cart-lateral .footer ul li span.frete .box-2').fadeOut(300, function () {
				$('#cart-lateral .footer ul li span.frete .box-1').fadeIn(300);
			});
		});
	},

	//CALCULA MANUALMENTE
	calculaFrete: function () {
		//MASK
		// $('#search-cep input[type="text"]').mask('00000-000');

		//CLICK
		$('#search-cep input[type=submit]').on('click', function (e) {
			e.preventDefault();
			if ($('#search-cep input[type="text"]').val().length === 9) {
				carrinhoFlutante.calculateShipping();
				$('#search-cep input[type="text"]').removeClass('active');
			} else {
				$('#search-cep input[type="text"]').addClass('active');
			}
		});

		//PRESS ENTER
		$('#search-cep input[type=text]').on('keypress', function (event) {
			if (keycode == '13') {
				if ($('#search-cep input[type="text"]').val().length === 9) {
					var keycode = (event.keyCode ? event.keyCode : event.which);
					carrinhoFlutante.calculateShipping();
					$('#search-cep input[type="text"]').removeClass('active');
				} else {
					$('#search-cep input[type="text"]').addClass('active');
				}
			}
		});
	},

	cartLateral: function () {
		vtexjs.checkout.getOrderForm()
			.done(function (orderForm) {
				//TOTAL CARRINHO
				var quantidade = 0;
				for (var i = orderForm.items.length - 1; i >= 0; i--) {
					quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
				}

				$('header .cart span').text(quantidade);

				//INFORMACOES DO CARRINHO
				if (orderForm.value != 0) {
					total_price = orderForm.value / 100;
					total_price = total_price.toFixed(2).replace('.', ',').toString();

					$('#cart-lateral .footer .total-price').text('R$: ' + total_price);
				} else {
					$('#cart-lateral .footer .total-price').text('R$: 0,00');
				}

				if (orderForm.totalizers.length != 0) {
					sub_price = orderForm.totalizers[0].value / 100;
					sub_price = sub_price.toFixed(2).replace('.', ',').toString();

					$('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: ' + sub_price);
				} else {
					$('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: 0,00');
				}

				if (orderForm.items != 0) {
					total_items = orderForm.items.length;

					$('#cart-lateral .header .total-items span').text(total_items + ' Itens');
				} else {
					$('#cart-lateral .header .total-items span').text('0 Itens');
				}
				//FIM - INFORMACOES DO CARRINHO

				//ITEMS DO CARRINHO
				$('#cart-lateral .content ul li').remove();
				for (i = 0; i < orderForm.items.length; i++) {

					price_item = orderForm.items[i].price / 100;
					price_item = price_item.toFixed(2).replace('.', ',').toString();
					id_prod = orderForm.items[i].id;
					

					var content = '';

					content += '<li class="cartItem" data-index="' + i + '" data-id="'+ id_prod +'">';
					content += '<div class="column_1"><img src="' + orderForm.items[i].imageUrl + '" alt="' + orderForm.items[i].name + '"/></div>';

					content += '<div class="column_2">';
					content += '<div class="name">';
					content += '<p>' + orderForm.items[i].name + '</p>';
					content += '</div>';

					content += '<div class="ft">';
					content += '<ul>';

					content += '<li class="price">';
					content += '<p>R$: ' + price_item + '</p>';
					content += '</li>';

					content += '<li data-index="' + i + '">';
					content += '<div class="box-count">';
					content += '<a href="" class="count count-down">-</a>';
					content += '<input type="text" value="' + orderForm.items[i].quantity + '" />';
					content += '<a href="" class="count count-up">+</a>';
					content += '</div>';
					content += '</li>';

					content += '<ul>';

					content += '</div>';
					content += '</div>';

					content += '<span class="removeUni">x</span>';
					content += '</li>';



					$('#cart-lateral .content > ul').append(content);
				}
				//FIM - ITEMS DO CARRINHO

				freteGratis();
			});
	},

	changeQuantity: function () {
		$(document).on('click', '#cart-lateral .columns>.column_2 .content ul li .column_2 .ft ul li .box-count .count', function (e) {
			e.preventDefault();

			var qtd = $(this).siblings('input[type="text"]').val();
			if ($(this).hasClass('count-up')) {
				qtd++
				$(this).siblings('input[type="text"]').removeClass('active');
				$(this).siblings('input[type="text"]').val(qtd);
			} else if ($(this).hasClass('count-down')) {
				if ($(this).siblings('input[type="text"]').val() != 1) {
					qtd--
					$(this).siblings('input[type="text"]').val(qtd);
				} else {
					//ALERTA 0 USUARIO QUANTIDADE NEGATIVA
					$(this).siblings('input[type="text"]').addClass('active');
				}
			}

			var data_index = $(this).parents('li').data('index');
			var data_quantity = $(this).parents('li').find('.box-count input[type="text"]').val();

			vtexjs.checkout.getOrderForm()
				.then(function (orderForm) {
					var total_produtos = parseInt(orderForm.items.length);
					vtexjs.checkout.getOrderForm()
						.then(function (orderForm) {
							var itemIndex = data_index;
							var item = orderForm.items[itemIndex];

							var updateItem = {
								index: data_index,
								quantity: data_quantity
							};

							return vtexjs.checkout.updateItems([updateItem], null, false);
						})
						.done(function (orderForm) {
							carrinhoFlutante.cartLateral();
						});
				});
		});
	},

	//OPEN QUANTITY
	openQuantity: function () {
		$(document).on('click', '#cart-lateral .ft .btn', function () {
			$(this).toggleClass('active');
			$(this).next('ul').slideToggle();
		});
	},

	removeItems: function () {
		$(document).on('click', '#cart-lateral .columns > .column_2 .content ul li .removeUni', function () {

			var data_index = $(this).parents('li').data('index');
			var data_quantity = $(this).siblings('li').find('.box-count input[type="text"]').val();

			vtexjs.checkout.getOrderForm()
				.then(function (orderForm) {
					var itemIndex = data_index;
					var item = orderForm.items[itemIndex];
					var itemsToRemove = [{
						"index": data_index,
						"quantity": data_quantity,
					}]
					return vtexjs.checkout.removeItems(itemsToRemove);
				})
				.done(function (orderForm) {
					carrinhoFlutante.cartLateral();
					setTimeout(() => {
						
						if ($('#cart-lateral .columns>.column_2 .content ul li').length < 1) {
							$('.freteGratis').addClass('semFrete');
							$('.freteGratis').removeAttr('style');
							$('.freteGratis').removeClass('conseguiu');
							$(".freteGratis .fraseFrete2").html('Seu carrinho está vazio, acumulando R$ 500,00 reais em compra, <span>o frete será gratuito!</span>');
						} else{
							freteGratis();
						}

					}, 2000);
				});
				
		});
	},

	removeAllItems: function () {
		$('#cart-lateral .clear').on('click', function () {
			vtexjs.checkout.removeAllItems()
				.done(function (orderForm) {
					//ATUALIZA O CARRINHO APÓS ESVAZIAR
					carrinhoFlutante.cartLateral();
				});
		});
	},

	add_to_cart: function () {
		$(document).on('click', '.prateleira-basica .sprite-comprar-async', function () {

			$(this).addClass('loading');

			var prodSku = $(this).parents('.comprar-produto').find('.sku.estoque-true.ativo').data('sku');
			

			vtexjs.catalog.getProductWithVariations($(this).parents('article').data('id')).done(function (product) {
				console.log(product);

				var item = {
					id: prodSku,
					quantity: 1,
					seller: '1'
				};

				vtexjs.checkout.getOrderForm()
				vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {

					$('.prateleira .prateleira-cta').removeClass('loading');

					carrinhoFlutante.cartLateral();

					//ATIVA CARRINHO LATERAL
					$('header .cart a').trigger('click');
				});
			});

		});
	},

	btn_buy: function () {
		window.alert = function () {
			$('header .cart a').trigger('click');
			carrinhoFlutante.cartLateral();
		}
	},

	openCart: function () {
		// $('#cart-lateral .header').on('click', function () {
		// 	$('#cart-lateral, #overlay').toggleClass('active');
		// });

		$('#overlay, #cart-lateral .header .close').on('click', function () {
			if ($('#cart-lateral').hasClass('active')) {
				
				$('#cart-lateral, #overlay').removeClass('active');
			}
		});
	}
}

function ajaxCallback() {
	console.log("teste");
	$('.search-multiple-navigator input[type="checkbox"]').vtexSmartResearch({
		shelfCallback: function () {
			console.log('shelfCallback');
		},

		ajaxCallback: function () {
			console.log('ajaxCallback');
			ameCashBack();
			carrinhoFlutante.add_to_cart();
		}
	});
}

$(document).ready(function() {

	carrinhoFlutante.cartLateral();
	carrinhoFlutante.toggle_carrinho();

	//FRETE
	// carrinhoFlutante.calculateShipping();
	// carrinhoFlutante.automaticCalculateShipping();
	// carrinhoFlutante.fadeAction();
	// carrinhoFlutante.calculaFrete();
	//FIM - FRETE

	carrinhoFlutante.changeQuantity();
	carrinhoFlutante.openQuantity();
	carrinhoFlutante.removeItems();
	carrinhoFlutante.removeAllItems();
	carrinhoFlutante.add_to_cart();
	carrinhoFlutante.btn_buy();
	carrinhoFlutante.openCart();

	if ($('body').hasClass('departamento')){
    	ajaxCallback();
	}

	$('.ame_cashback').each(function(index, element) {
		var id_prat = $(this).data('id');

		vtexjs.catalog.getProductWithVariations(id_prat).done(function(product) {
			$.each(product.skus, function (index, sku) {
				if (sku.available != false) {
					var preco = sku.bestPrice;
					var percentage = (preco * 30) / 100;
					var percentage_number = (percentage * 100) / preco;
					var money_percent = formatReal(percentage);
					
					// console.log(preco);
					// console.log(percentage);
					// console.log(percentage_number);
					// console.log(money_percent);
					// console.log(element);
					
					$(element).find('span#cash_back').html("R$" + money_percent);
				}
			});
		});
	});
	
	if ($('body').hasClass('produto')) {
		$.each(skuJson.skus, function (index, sku){
		    if (sku.available != false) {
				var preco = sku.bestPrice
				var percentage = (preco * 30) / 100;
				var percentage_number = (percentage * 100) / preco;
				var money_percent = formatReal(percentage);
			
				// console.log(preco);
				// console.log(percentage);
				// console.log(percentage_number);
				// console.log(money_percent);
				// console.log(element);
			
				$(".cashback_ame").css("display", "inline-flex");
				$('.cashback_ame .cash_back').html("R$" + money_percent);
		    }
		});
	}
	
	$("h2:contains('DESCRIÇÃO DO PRODUTO')").css('font-weight', 'bold');

	$('.compre-look a').on('click', function(e){
		e.preventDefault();
		var thisId = $(this).data('modal-id');

		console.log(thisId);

		$('body').append('<div class="compre-look-overlay"></div>');
		$('#'+thisId).addClass('active');
	});

	$(document).on('click','.compre-look-overlay', function(){
		$('.modal').removeClass('active');
		$(this).remove();
	});

	var random = Math.floor(Math.random() * $('.socials-random').length);
	$('.socials-random').hide().eq(random).show();


	if($('body.new-home').length) {

		$('ul .categoria').hover(
			function(e){
				if ($(this).children('.ul-categoria').length) {
					$(this).children('.ul-categoria').addClass('active');
				}
			},
			function(e){
				$(this).children('.ul-categoria').removeClass('active');
			}
		);

		$('.ul-li-categoria').hover(
			function(e){
				if($(this).children('.options-categoria').length){
					$(this).parents('.ul-categoria').addClass('box-shadow');
					$(this).children('.options-categoria').addClass('active');
				}
			},
			function(e){
				$(this).children('.options-categoria').removeClass('active');
				$(this).parents('.ul-categoria').removeClass('box-shadow');
			}
		);
	}


	$('footer .form-newsletter').on("submit", function(e) {
		e.preventDefault();
		var sexo = $('input[name="genero"]:checked').val();
		var nome = $('input[name="FormValue_Fields[CustomField4410]"]').val();
		var email = $('input[name="FormValue_Fields[EmailAddress]"]').val();


		var obj_dados = {
			"sexo" : sexo,
			"nome" : nome,
			"email"  : email
		}
		var json_dados = JSON.stringify(obj_dados);
        console.log(obj_dados);
		
		setTimeout(function() {
			$.ajax({
				url: '/api/dataentities/CN/search?_fields=email&email='+ email,
				type: 'GET'
			}).done(function(res) {
				if (res.length > 1) {
					alert("E-mail já cadastrado, por favor use um e-mail diferente");
					
					$('.form-newsletter').trigger("reset");					
				} else {
					insertMasterData("CN", 'pierrecardin', json_dados, function(res) {
			        	console.log(res);
			        	setTimeout(function() {
			
							jQuery.support.cors = true;
							$.ajax({
							    url: 'https://news.mailclick.me/subscribe.php',
							    type: 'post',
							    data: {
							     "FormValue_Fields[EmailAddress]": email,
							     "FormValue_Fields[CustomField4410]": name,
							     "FormValue_ListID": 4632,
							     "FormValue_Command": "Subscriber.Add",
							     "FormButton_Subscribe": "OK"
							    },
							    success: function(res) {
							   console.log('mlbz ok');
							    }
							  });
			
			        		$('.radio-news').remove();
			        		$('input[name="FormValue_Fields[CustomField4410]"]').remove();
			        		$('input[name="FormValue_Fields[EmailAddress]"]').remove();
			        		$('input[type="submit"]').remove();
			        		$('.form-newsletter').append('<div class="full msg_sucesso">Cadastro feito com sucesso!</div>');
			        	}, 1000);
			        	setTimeout(function() {
			        		// $('.msg_sucesso').remove();
			        	}, 10000);
			        });
				}
			});
		}, 1000);
		
		
	});
	//pagina pre-home
	$('li.helperComplement').remove();

	if($('.async').length > 0){
		setTimeout(function() {
			$('li.helperComplement').remove();
			var elems = $('.async li');
			var arr = $.makeArray(elems);
			$.each( arr, function( r, x ){
				// console.log('ab');

				if ( $(x).find('.produto').data('id') != undefined) {
					var id = $(x).find('.produto').data('id');

					// console.log(id);

					if($(x).find('.comprar-produto .skus').length>0){

					}else{
						$(x).find('.comprar-produto').append('<div class="skus full"><p>Selecione a opÃƒÂ¯Ã‚Â¿Ã‚Â½ÃƒÂ¯Ã‚Â¿Ã‚Â½o:</p>');
						$(x).find('.comprar-produto').append('<div class="escolhas full">');
						setTimeout(function(){
							// console.log(id);
							vtexjs.catalog.getProductWithVariations(id).done(function(product){
								var skus_qtd = product.skus.length;
								if(skus_qtd > 8){
									$(x).find('.comprar-produto .skus.full').append('<select class="escolha_sku ativo">')
									for (var c = 0; c < skus_qtd; c++) {
										var nome = product.skus[c].dimensions.Tamanho;
										$(x).find('.comprar-produto .skus.full select.ativo').append('<option data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'" val="'+product.skus[c].skuname+'">'+nome+'');
									}
									$('.comprar-produto .skus.full select').change(function(event) {
										var sku_select = $(this).find('option:selected').attr('data-sku');
										var seller_select = $(this).find('option:selected').attr('data-seller');
										$(this).attr('data-sku', sku_select);
										$(this).attr('data-seller', seller_select);
									});
								}else{
									for (var c = 0; c < skus_qtd; c++) {
									var nome = product.skus[c].dimensions.Tamanho;
									var estoque = product.skus[c].available;
										if(c == 0){
											$(x).find('.comprar-produto .skus.full').append('<div class="sku ativo sku-'+nome+' estoque-'+estoque+'" data-id="'+product.productId+'" data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'">'+nome+'');
										}else{
											$(x).find('.comprar-produto .skus.full').append('<div class="sku sku-'+nome+' estoque-'+estoque+'" data-id="'+product.productId+'"  data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'">'+nome+'');
										}
										// $('.sku.sku-P').trigger('click');
									}
								}
								// $(x).find('.comprar-produto .escolhas.full').append('<div class="quantidade"><div class="sprite-mais" onclick="adicionar(event)"></div><div class="sprite-menos" onclick="tirar(event)"></div>');
								$(x).find('.comprar-produto .escolhas.full').append('<a href="javascript:void(0)" class="sprite-comprar-async" data-sku="">COMPRAR');
								$(x).find('.comprar-produto .escolhas.full').append('<div class="produto_esgostado" style="display:none;">Produto Esgotado</div>');
							});
						},500);
					}
				}



			});

		},500);
	}


	if($('.forher').length > 0){
		$.cookie('sexo', 'menina' , { expires: 7, path: '/' });
	}else if($('.forhim').length > 0){
		$.cookie('sexo', 'menino' , { expires: 7, path: '/' });
	}


	if($('body#home-page').length > 0){

		if($.cookie('sexo') == 'menina'){
			window.location.href = '/forher';
		}else if($.cookie('sexo') == 'menino'){
			window.location.href = '/forhim';
		}else{
			$('body').fadeIn('slow', function() {});
		}

		$( "#hover-forher" ).hover(
	  function() {
		$(this).addClass('bg-modal');
		$("#content-forher").css('opacity','1');
	   }, function(e) {
		$(this).removeClass('bg-modal');
		$("#content-forher").css('opacity','0');
    });
	$( "#hover-forhim" ).hover(
	  function() {
		$(this).addClass('bg-modal');
		$("#content-forhim").css('opacity','1');
	   }, function() {
		$(this).removeClass('bg-modal');
		$("#content-forhim").css('opacity','0');
    });
	}

	if($('.forher').length > 0){
		$.cookie('sexo', 'menina' , { expires: 7, path: '/' });
	}else if($('.forhim').length > 0){
		$.cookie('sexo', 'menino' , { expires: 7, path: '/' });
	}

	if($('body.home').length > 0){

		if($.cookie('popup') == 'apareceu'){
			$.cookie('popup', 'apareceu', { expires: 7, path: '/' })
		}else{
			$('.blc_newsletter.all').fadeIn('400', function() {

	        	setTimeout(function() {
					$('.fechar').click(function(event) {
						$('.blc_newsletter.all').fadeOut('400')
						/* Act on the event */
					});
	        	}, 500);
				$('.central.newsletter').center()
				$('.blc_newsletter.all .form-newsletter').on("submit", function(e) {
					e.preventDefault();
					var sexo = $('.all input[name="genero"]:checked').val();
					var nome = $('.all input[name="nome"]').val();
					var email = $('.all input[name="email"]').val();

					var obj_dados = {
						"sexo" : sexo,
						"nome" : nome,
						"email"  : email
					}
					var json_dados = JSON.stringify(obj_dados);
			        console.log(obj_dados);

					insertMasterData("CN", 'pierrecardin', json_dados, function(res) {
			        	console.log(res);
			        	setTimeout(function() {
			        		$('.radio-news').remove();
			        		$('input[name="nome"]').remove();
			        		$('input[name="email"]').remove();
			        		$('input[type="submit"]').remove();
			        	}, 1000);
			        	setTimeout(function() {
			        		// $('.msg_sucesso').remove();
			        	}, 10000);
			        });
				});
			});
		}


		// FULL BANNER - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
		$('.banner .ajusta_banner').cycle({
		    fx:     'fade',
		    speed:  'slow',
		    pause: 	 true,
		    timeout: 6000,
		    next:   '#next',
		    prev:   '#prev'
		});
		// FULL BANNER - FIM

		// CARROSSEL DE PRODUTOS - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
		$('.jcarousel .prateleira-basica > h2').remove();
		$('.jcarousel .helperComplement').remove();
		// $('.jcarousel > div > div').jcarousel();
		// $('.jcarousel:eq(0)').jcarousel();
		// $('.jcarousel-control-prev')
		//     .on('jcarouselcontrol:active', function() {
		//         $(this).removeClass('inactive');
		//     })
		//     .on('jcarouselcontrol:inactive', function() {
		//         $(this).addClass('inactive');
		//     })
		//     .jcarouselControl({
		//         target: '-=1'
		//     });
		// 
		// $('.jcarousel-control-next')
		//     .on('jcarouselcontrol:active', function() {
		//         $(this).removeClass('inactive');
		//     })
		//     .on('jcarouselcontrol:inactive', function() {
		//         $(this).addClass('inactive');
		//     })
		//     .jcarouselControl({
		//         target: '+=1'
		//     });
		// CARROSSEL DE PRODUTOS - FIM


		// HOVER MINIBANNER - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
		var elems = $('.mini-banner .box-banner');
		var arr = $.makeArray(elems)
		$.each( arr, function( r, x ){
			var hover = $(x).find('img').attr('src').split('/')[6].split('.')[0];
			$(x).find('img').after('<img src="http://pierrecardin.vteximg.com.br/arquivos/'+hover+'_hover.png" class="hover_img" />')
		});
		// HOVER MINIBANNER - FIM
	};


	// KIT LOOK - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
	if($('body.produto.produto-look').length > 0){
		var elems = $('.full.kit');
		var arr = $.makeArray(elems)
		$.each( arr, function( r, x ){
			if($(x).find('.apresentacao').length > 0){
				$('[class*=skuespec]').click(function(event) {
					if($(this).parents('.full.kit').find('.select_kit').hasClass('ativo')){
						var $obj = $(this);
	            		setTimeout(function(){
							var numeral = $obj.parents('.full.kit').find('.skuPrice').text();
		            		$obj.parents('.full.kit').attr('data-valor', apenasNumeros(numeral));
		            		somaManos()
						}, 300);
					}
				});
				$('.select_kit').click(function(){
					if($(this).hasClass('ativo')){
		            	$(this).removeClass('ativo');
		            	$(this).parent('.full.kit').removeClass('ativo');
		            	$(this).parent('.full.kit').removeAttr('data-valor');
					}else{
		            	var numeral = $(this).parent('.full.kit').find('.skuPrice').text()
		            	$(this).parent('.full.kit').attr('data-valor', apenasNumeros(numeral))
		            	$(this).addClass('ativo');
		            	$(this).parent('.full.kit').addClass('ativo');
		            	$(this).parent('.full.kit').find('[class*=skuespec]:eq(0)').trigger('click')
		            	somaManos()
					}
			    });
			    $('.select_kit.ativo').click(function(){
		            $(this).removeClass('ativo');
			    });

				if($(x).find('.apresentacao').length > 3){
					$('.segura_kits').mCustomScrollbar({
						scrollButtons:{enable:true},
						theme:"dark-thin",
						scrollbarPosition:"outside"
					});
				}
			}else{
				$(x).remove()
			}
		});
		// $('.buy-button').attr('href', '/checkout/cart/add?' )
		$('.buy-button').attr('href', '#' )
		$('.buy-button').click(function(event) {
			var selects = $('.select_kit.ativo').length
			var link = '';
			// console.log(selects);
			for(i=0; i<= selects; ){
				link += '&';
				link += $('.segura_kits .right .buy-in-page-button:eq('+i+')').attr('href').split('/')[3].split('?')[1].split('&')[0];
				link += '&';
				link += $('.segura_kits .right .buy-in-page-button:eq('+i+')').attr('href').split('/')[3].split('?')[1].split('&')[1];
				link += '&';
				link += $('.segura_kits .right .buy-in-page-button:eq('+i+')').attr('href').split('/')[3].split('?')[1].split('&')[2];
				// console.log(link);
				i++
				if(i==selects){
					// console.log('acabou');
					window.location.href = '/checkout/cart/add?'+link

				}
			}
		});
	};
	// KIT LOOK - FIM

	// FUNÃƒÂ¯Ã‚Â¿Ã‚Â½ÃƒÂ¯Ã‚Â¿Ã‚Â½ES PÃƒÂ¯Ã‚Â¿Ã‚Â½GINA DE PRODUTO - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
	if($('body.produto').length > 0){
		var elems = $('.preco .skuList');
		var arr = $.makeArray(elems)
		$.each( arr, function( r, x ){
			$(x).find('.imageSku').css('left', r*55+'px');
			if(r==0){
				$(x).addClass('ativo');
			}
			$(x).find('.imageSku').click(function(event) {
				$('.skuList').removeClass('ativo');
				$(this).parents(x).addClass('ativo');
			});
		});

		$('.abre-fecha').click(function(event) {
			if($(this).hasClass('fechadinho')){
				$(this).removeClass('fechadinho');
				$('.produto_flutuante').removeClass('fechada');
			}else{
				$('.produto_flutuante').addClass('fechada');
				$(this).addClass('fechadinho')
			}
		});

		// // MEDIDAS - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
		// var medida = $('.medida img').attr('src');
		// // console.log(medida);
		// if (medida == undefined) {
		// 	$('.medida').remove();
		// }else {
		// 	$('.medida > a').attr('href', medida);

		// 	$('.medida > a').fancybox();
		// }
		// // MEDIDAS - FIM

		// CARROSSEL RELACIONADOS
		$('.jcarousel .prateleira-basica > h2').remove();
		$('.jcarousel .helperComplement').remove();
		// $('.jcarousel > div > div').jcarousel();
		// $('.jcarousel:eq(0)').jcarousel();
		// $('.jcarousel-control-prev')
		//     .on('jcarouselcontrol:active', function() {
		//         $(this).removeClass('inactive');
		//     })
		//     .on('jcarouselcontrol:inactive', function() {
		//         $(this).addClass('inactive');
		//     })
		//     .jcarouselControl({
		//         target: '-=1'
		//     });
		// 
		// $('.jcarousel-control-next')
		//     .on('jcarouselcontrol:active', function() {
		//         $(this).removeClass('inactive');
		//     })
		//     .on('jcarouselcontrol:inactive', function() {
		//         $(this).addClass('inactive');
		//     })
		//     .jcarouselControl({
		//         target: '+=1'
		//     });
	};
	// FUNÃƒÂ¯Ã‚Â¿Ã‚Â½ÃƒÂ¯Ã‚Â¿Ã‚Â½ES PÃƒÂ¯Ã‚Â¿Ã‚Â½GINA DE PRODUTO - FIM



	// BOTÃƒÂ¯Ã‚Â¿Ã‚Â½O ESPIAR - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
	$("a.fancybox").fancybox({
		width: 1200,
		minHeight: 450,
		type: "iframe"
	});
	// BOTÃƒÂ¯Ã‚Â¿Ã‚Â½O ESPIAR - FIM



	// FAIXA DE PREÃƒÂ¯Ã‚Â¿Ã‚Â½O - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
	var slider = document.getElementById('slider-range');
	if($('.HideFaixa-de-preco').length > 0){
		// console.log(inicio_valor,final_valor)
		noUiSlider.create(slider, {
			connect: true,
			start: [1, 500],
			range: {
				'min': 1,
				'max': 500
			}
		});
		$('#slider-range').append('<span class="value-min">0,00</span><span class="value-max">500,00</span>');

		$('#slider-range').after('<a onclick="compor()" class="filtrar">FILTRAR PREÃƒÂ¯Ã‚Â¿Ã‚Â½O</a>');


		slider.noUiSlider.on('slide', function(){
			var valor_porcento_1 = slider.noUiSlider.get()[0]
			$('span.value-min').text(slider.noUiSlider.get()[0]);
			$('span.value-max').text(slider.noUiSlider.get()[1]);
			$('.filtrar').attr('minimo', slider.noUiSlider.get()[0].replace(".",","))
			$('.filtrar').attr('maximo', slider.noUiSlider.get()[1].replace(".",","))
		});
	}
	// FAIXA DE PREÃƒÂ¯Ã‚Â¿Ã‚Â½O - FIM



	if($('body.central-atendimento').length > 0){
		$('.imagensloja .box-banner a').attr('rel', 'group');
		$('.imagensloja .box-banner a').fancybox();
		$('#accordion').accordion({
			collapsible: true,
			active: false,
	    	heightStyle: "content"
		});
		$('#accordion2').accordion({
			collapsible: true,
			active: false,
	    	heightStyle: "content"
		});
	};

	// FORMAS DE PARCELAMENTO
	var abre_parcelas = 0;
	setTimeout(function(){
		$('.titulo-parcelamento').click(function(){
	        if(abre_parcelas == 1){
	            // $('.titulo-parcelamento').removeClass('ativo');
	            $('.other-payment-method-ul').css('height', '0');
	            // console.log('meu');
	            abre_parcelas--;
	        }else{
	            // $('.titulo-parcelamento').addClass('ativo');
	            $('.other-payment-method-ul').css('height', '200px');
	            // console.log('teu');
	            abre_parcelas++;
	        }
	    });
	}, 3000);
	// FORMAS DE PARCELAMENTO


	// HOVER MENU HEADER
	$(".hover_menu").hoverIntent(config);
	// HOVER MENU HEADER


	$('.shipping-value').trigger('click');

	$('.helperComplement').remove();
});


$(window).load(function(){
$('#instafeed a img').centerImage();

	$(document).on('click','.sku', function(event) {
		$(event.target).parent('.skus').find('.sku').removeClass('ativo');
		var id = $(this).data('id');
		// console.log($(this).data('id'));
		var sku = parseInt($(this).index())-parseInt(1);
		$(this).addClass('ativo')
		vtexjs.catalog.getProductWithVariations(id).done(function(product){
			// console.log(product.skus[sku]);
			if(product.skus[sku].available == false){
				// console.log('mano oi');
				// $(event.target).parents('.comprar-produto').find('.escolhas').css('display', 'none');
				$(event.target).parents('.comprar-produto').find('.escolhas > p').css('display', 'none');
				$(event.target).parents('.comprar-produto').find('.escolhas .quantidade').css('display', 'none');
				$(event.target).parents('.comprar-produto').find('.escolhas .sprite-comprar-async').css('display', 'none');

				//$(event.target).parents('.comprar-produto').find('.escolhas .produto_esgostado').css('display', 'block');
			}else{
				// $(event.target).parents('.comprar-produto').find('.escolhas').css('display', 'block');
				$(event.target).parents('.comprar-produto').find('.escolhas > p').css('display', 'block');
				// $(event.target).parents('.comprar-produto').find('.escolhas .quantidade').css('display', 'block');
				$(event.target).parents('.comprar-produto').find('.escolhas .sprite-comprar-async').css('display', 'block');

				$(event.target).parents('.comprar-produto').find('.escolhas .produto_esgostado').css('display', 'none');

				$(event.target).parents('.full.preco').find('.valor-de em').text(product.skus[sku].listPriceFormated);
				$(event.target).parents('.full.preco').find('.valor-por strong').text(product.skus[sku].bestPriceFormated);
			}
		})
	});
	vtexjs.catalog.getCurrentProductWithVariations().done(function(product){
	    var parcela = product.skus[0].installments;
	    if(parcela == 1){
	    	$('body.produto .productPrice .valor-por').attr('style', 'display: inline !important');
	    }
	});
});

$(document).ajaxStop(function() {

	$('.sku').click(function(event) {
		$(this).parent('.skus').find('.sku').removeClass('ativo');
		var id = $(this).data('id');
		// console.log($(this).data('id'));
		var sku = parseInt($(this).index())-parseInt(1);
		$(this).addClass('ativo');
		vtexjs.catalog.getProductWithVariations(id).done(function(product){
			// console.log(product.skus[sku]);
			if(product.skus[sku].available === false){
				// console.log('mano');
				// $(event.target).parents('.comprar-produto').find('.escolhas').css('display', 'none');

				$(this).parents('.comprar-produto').find('.escolhas > p').css('display', 'none');
				$(this).parents('.comprar-produto').find('.escolhas .quantidade').css('display', 'none');
				$(this).parents('.comprar-produto').find('.escolhas .sprite-comprar-async').css('display', 'none');

				//$(event.target).parents('.comprar-produto').find('.escolhas .produto_esgostado').css('display', 'block');
			}else{
				// $(event.target).parents('.comprar-produto').find('.escolhas').css('display', 'block');

				$(this).parents('.comprar-produto').find('.escolhas > p').css('display', 'block');
				$(this).parents('.comprar-produto').find('.escolhas .sprite-comprar-async').css('display', 'block');

				$(this).parents('.comprar-produto').find('.escolhas .produto_esgostado').css('display', 'none');

				$(this).parents('.full.preco').find('.valor-de em').text(product.skus[sku].listPriceFormated);
				$(this).parents('.full.preco').find('.valor-por strong').text(product.skus[sku].bestPriceFormated);
			}
		})
	});

	$('li.helperComplement').remove();

	if($('.async').length > 0){
		setTimeout(function() {
			$('li.helperComplement').remove();
			var elems = $('.async li');
			var arr = $.makeArray(elems);
			$.each( arr, function( r, x ){
				// console.log('ab');
				var id = $(x).find('.produto').data('id');
				if($(x).find('.comprar-produto .skus').length>0){

				}else{
					$(x).find('.comprar-produto').append('<div class="skus full"><p>Selecione a opÃƒÂ¯Ã‚Â¿Ã‚Â½ÃƒÂ¯Ã‚Â¿Ã‚Â½o:</p>');
					$(x).find('.comprar-produto').append('<div class="escolhas full">');
					setTimeout(function(){
						vtexjs.catalog.getProductWithVariations(id).done(function(product){
							var skus_qtd = product.skus.length;
							if(skus_qtd > 8){
								$(x).find('.comprar-produto .skus.full').append('<select class="escolha_sku ativo">')
								for (var c = 0; c < skus_qtd; c++) {
									var nome = product.skus[c].dimensions.Tamanho;
						    		$(x).find('.comprar-produto .skus.full select.ativo').append('<option data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'" val="'+product.skus[c].skuname+'">'+nome+'');
								}
								$('.comprar-produto .skus.full select').change(function(event) {
									var sku_select = $(this).find('option:selected').attr('data-sku');
									var seller_select = $(this).find('option:selected').attr('data-seller');
									$(this).attr('data-sku', sku_select);
									$(this).attr('data-seller', seller_select);
								});
							}else{
								for (var c = 0; c < skus_qtd; c++) {
								var nome = product.skus[c].dimensions.Tamanho;
								var estoque = product.skus[c].available;
									if(c == 0){
							    		$(x).find('.comprar-produto .skus.full').append('<div class="sku ativo sku-'+nome+' estoque-'+estoque+'" data-id="'+product.productId+'" data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'">'+nome+'');
									}else{
							    		$(x).find('.comprar-produto .skus.full').append('<div class="sku sku-'+nome+' estoque-'+estoque+'" data-id="'+product.productId+'"  data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'">'+nome+'');
									}
									// $('.sku.sku-P').trigger('click');
								}
							}
					    	// $(x).find('.comprar-produto .escolhas.full').append('<div class="quantidade"><div class="sprite-mais" onclick="adicionar(event)"></div><div class="sprite-menos" onclick="tirar(event)"></div>');
					    	$(x).find('.comprar-produto .escolhas.full').append('<a href="javascript:void(0)" class="sprite-comprar-async">COMPRAR');
					    	$(x).find('.comprar-produto .escolhas.full').append('<div class="produto_esgostado" style="display:none;">Produto Esgotado</div>');
						});
					},500);
				}

			});

		},500);
	}

	var setSize = function () {  //define a function with the code you want to call
		$( ".banner" ).css( "height", $('.banner img:visible').height() );
		// console.log('mano')
	};

	$(window).resize(setSize);  //set the function to resize
	setSize();  //call the function now

	$('.menu-plus .departamentos li').click(function(event) {
		// console.log($(this).index());
		$('.menu-plus .departamentos').addClass('remove');
		$('.menu-plus h2').text('CATEGORIAS');
		$('.menu-plus h2').after('<span class="fechar">VOLTAR</span>');
		$('.menu-plus ul:eq(1) > li').css('display','none');
		$('.menu-plus ul:eq(1) > li:eq('+$(this).index()+')').css('display','block');
		$('.fechar').click(function(event) {
			$('.menu-plus h2').text('DEPARTAMENTOS');
			$('.menu-plus .fechar').remove();
			$('.menu-plus .departamentos').removeClass('remove');
		});
	});
});



function compor(){
	var url = window.location.pathname
	var parts = url.split("/");
	var mano = parts[parts.length-1]
    // console.log();
	var final = $('.HideFaixa-de-preco + ul li:eq(0) a').attr('href').split('?')[1]
	// console.log(final);
	if(mano.split('e')[0] == 'd'){
		window.location.href = 'http://'+window.location.hostname+''+window.location.pathname.replace(mano, '/de-'+$('.filtrar').attr('minimo')+'-a-'+$('.filtrar').attr('maximo')+'?'+final+'')
		//window.location.href = 'http://'+window.location.hostname+'/'+parts[parts.length-2]+'/de-'+$('.filtrar').attr('minimo')+'-a-'+$('.filtrar').attr('maximo')+'?PS=12&map=c,priceFrom';
		//console.log(window.location.hostname+'/'+parts[parts.length-2]+'/de-'+$('.filtrar').attr('minimo')+'-a-'+$('.filtrar').attr('maximo'))
	}else{
		window.location.href = 'http://'+window.location.hostname+''+window.location.pathname+'/de-'+$('.filtrar').attr('minimo')+'-a-'+$('.filtrar').attr('maximo')+'?'+final;
	}
}

// FUNÃƒÂ¯Ã‚Â¿Ã‚Â½ÃƒÂ¯Ã‚Â¿Ã‚Â½O PARA O HOVER DO MENU PRINCIPAL
function megaHoverOver(){
	$(this).find(".menu_hover").stop().fadeTo('fast', 1).show();
	$(".menu_hover").css({'z-index':'999999'});
}
function megaHoverOut(){
 $(this).find(".menu_hover").stop().fadeTo('fast', 0, function() {
	}).hide();
}
var config = {
	 sensitivity: 12, // number = sensitivity threshold (must be 1 or higher)
	 interval: 10, // number = milliseconds for onMouseOver polling interval
	 over: megaHoverOver, // function = onMouseOver callback (REQUIRED)
	 timeout: 150, // number = milliseconds delay before onMouseOut
	 out: megaHoverOut // function = onMouseOut callback (REQUIRED)
};





// MOBILE
$(document).ready(function() {


	$(window).resize(function() {
		var width = $(window).width();
    	var height = $(window).height();
    	var height_img = $('.banner .box-banner a img').height();
 	});

	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);


	if(check == true)	{
		if($('body.produto.desktop').length > 0){
			var urlPdf = window.location.href;
			window.location.assign(urlPdf+'?lid=b6b209d3-b1a9-4c5f-a6c7-fe9f98efa4d2');
		};

		var width = $(window).width();
	    var height = $(window).height();
	    var height_img = $('.banner .box-banner a img').height();
	    var menu_button = 65;
	    var menu_width = (width) - (menu_button);
	    var menu_topo = 0;
	    var filtro = 0;
	    var seta_desc = 0;
	    var menu_lista = 0;

	    // MENU MOBILE - INÃƒÂ¯Ã‚Â¿Ã‚Â½CIO
		$('.sprite-menu_mobile').click(function(){
	        if(menu_topo == 1){
	            $('.menu_mobile').removeClass('ativo');
	            $('.sprite-menu_mobile').removeClass('ativo');
	            $('.fundo_black').remove();
	            $('body').css('position', 'relative');
	            $('.menu_lista').removeClass('ativo');
   				$('.menu_lista .segura').css('height', '0');
	            menu_topo--;
	        }else{
	            $('.menu_mobile').addClass('ativo');
	            $('.sprite-menu_mobile').addClass('ativo');
	            $('header.mobile').prepend('<div class="fundo_black"></div>');
	            $('body').css('position', 'fixed');
	            menu_topo++;
	        }
	    });
		// MENU MOBILE - FIM

		// SUBMENU MOBILE
		$('.menu_lista').click(function() {
			if ($(this).hasClass('ativo')) {
				$('.menu_lista').removeClass('ativo');
				$('.menu_lista .segura').css('height', '0');
			}else{
				var altura_menu_lista = $(this).find('.links a').length*36;
				// console.log(altura_menu_lista);
	   			$('.menu_lista').removeClass('ativo');
	   			$('.menu_lista .segura').css('height', '0');
	   			$(this).addClass('ativo');
				$(this).find('.segura').css('height', ''+altura_menu_lista+'px');
				setTimeout(function(){
				}, 300);
			}
		});
		// SUBMENU MOBILE


		// FILTRO MOBILE - INSTITUCIONAL
		if($('.menu-atendimento').length > 0){
			$('.menu-atendimento').css('height', height-105);
		    $('.abre-filtro').click(function(){
		        if(filtro == 1){
		            $('.menu-atendimento').css('display', 'none');
		            $('body').css('position', 'relative');
		            filtro--;
		        }else{
		            $('.menu-atendimento').css('display', 'block');
		            $('body').css('position', 'fixed');
		            filtro++;
		        }
		    });
		};

	    if($('.menu-lateral').length > 0){
			$('.menu-lateral').css('height', height-105);
		    $('.abre-filtro').click(function(){
		        if(filtro == 1){
		            $('.menu-lateral').css('display', 'none');
		            $('body').css('position', 'relative');
		            filtro--;
		        }else{
		            $('.menu-lateral').css('display', 'block');
		            $('body').css('position', 'fixed');
		            filtro++;
		        }
		    });
		};
	}
	else
	{
	   // console.log('nÃƒÂ¯Ã‚Â¿Ã‚Â½o mobile');
	}

});
// MOBILE

	$(document).scroll(function() {
		if($(document).scrollTop() > 582){
			$('.produto_flutuante').css('right','0');
		} else {
			$('.produto_flutuante').css('right','-400px');
		}
	});

	$(document).ready(function(){
		$("a[href='#botao-subir']").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

    $(window).scroll(function() {

        if ($(this).scrollTop()>120)
         {
            $('a[href="#botao-subir"]').fadeIn();
         }
        else
         {
          $('a[href="#botao-subir"]').fadeOut();
         }
     });
});

function freteGratis() {
	$(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
        if (orderForm.totalizers.length > 0) {
            var desconto2 = orderForm.totalizers[0].value;
            var finalPromo2 = 50000 - desconto2;
            var valor_porcento2 = (desconto2 / 50000) * 100;
            var teste2 = finalPromo2 / 100;
            var finalFeliz2 = teste2.toFixed(2);
            var finalFeliz2 = finalFeliz2.replace(".", ",");
			var finalSplit2 = (desconto2 / 100);
			// console.log(desconto2);
			
			

            if (desconto2 < 50000) {
				$('.freteGratis').removeClass('conseguiu');
				$('.freteGratis').removeClass('semFrete');

                $('.freteGratis').css({
					'background': '-moz-linear-gradient(90deg, rgba(245,124,34,1) 0%, rgba(253,192,104,1) '+ Math.round(valor_porcento2) +'%, rgba(0,0,0,1) '+ Math.round(valor_porcento2) +'%, rgba(80,80,80,1) 100%)',
    				'background': '-webkit-linear-gradient(90deg, rgba(245,124,34,1) 0%, rgba(253,192,104,1) '+ Math.round(valor_porcento2) +'%, rgba(0,0,0,1) '+ Math.round(valor_porcento2) +'%, rgba(80,80,80,1) 100%)',
					'background': 'linear-gradient(90deg, rgba(245,124,34,1) 0%, rgba(253,192,104,1) '+ Math.round(valor_porcento2) +'%, rgba(0,0,0,1) '+ Math.round(valor_porcento2) +'%, rgba(80,80,80,1) 100%)'
				});
				$(".freteGratis .fraseFrete2").html('Com mais R$ ' + finalFeliz2 + ' <span>  o frete é por nossa conta!</span>');

            } else {
				$('.freteGratis').addClass('conseguiu');
				$('.freteGratis').removeAttr('style');
                $(".freteGratis .fraseFrete2").html('Parabéns! Você ganhou o Frete Grátis!');
			}

        }
    });
}

function instaFeed (username) {
    $.instagramFeed({
        'username': username,
        'container': '#instafeed',
        'display_profile': false,
        'display_biography': false,
        'display_gallery': true,
        'get_raw_json': true,
        'callback': function (data) {
            data = $.parseJSON(data);
            let post = data.images;

            $.each(post, function (index, item) {
                if (index < 4){
                    $('<div><a href="https://www.instagram.com/p/' + item.node.shortcode + '" target="_blank"><img src="' + item.node.thumbnail_src + '"/></a></div>').appendTo('#instafeed');
                }
            });

            //SLICK PARA MOBILE
            // if ($('body').width() < 810) {
            //     $('#instagram ul').slick({
            //         dots: false,
            //         arrows: false,
            //         infinite: true,
            //         autoplay: 3000,
            //         slidesToShow: 1,
            //         slidesToScroll: 1
            //     });
            // }
        },
        'styling': false,
        'items': 4,
        'items_per_row': 4,
        'margin': 0
    });
};

$(document).ready(function() {

	instaFeed('pierrecardinforhim');

	freteGratis();

	$('.lancamentos .divHeader .seeAll').attr('href', 'https://store.pierrecardin.com.br/masculino?PS=12&O=OrderByReleaseDateDESC');
	
	$('.destaques .divHeader .seeAll').attr('href', 'https://store.pierrecardin.com.br/masculino/147?PS=12&map=c,productClusterIds&O=OrderByTopSaleDESC');

	$(window).scroll(function(){
		var header = $('header.desktop');
		scroll = $(window).scrollTop();
		
		if (scroll >= 25) {
			header.addClass('fixed');
		} else {
			header.removeClass('fixed');
		}
	});

});
// lari