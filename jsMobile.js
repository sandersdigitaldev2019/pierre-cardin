$(document).ready(function() {
    var header = {
        'Accept': 'application/json',
        'REST-range': 'resources=0-100',
        'Content-Type': 'application/json; charset=utf-8'
    };
    
    // testes testando
    
    var selectMasterData = function(ENT, params, fn) {
        // Consulta dados na master data
        $.ajax({
            url: '/api/dataentities/' + ENT + '/search?' + params,
            type: 'GET',
            headers: header,
            success: function(res) {
                fn(res);
            },
            error: function(res) {
            }
        });
    };
    
    var insertMasterData = function(ENT, dados, fn) {
        $.ajax({
            url: '/api/dataentities/' + ENT + '/documents/',
            type: 'PATCH',
            data: dados,
            headers: header,
            success: function(res) {
                fn(res);
            },
            error: function(res) {
            }
        });
    };
    
    //CADASTRO NEWLETTER
    $('footer .form-newsletter').on("submit", function(e) {
        e.preventDefault();
        var nome = $('input[name="nome"]').val();
        var email = $('input[name="email"]').val();
        
        var obj_dados = {
            "nome" : nome,
            "email"  : email
        }
        var json_dados = JSON.stringify(obj_dados);
        console.log(obj_dados);
        
        setTimeout(function() {
			$.ajax({
				url: '/api/dataentities/CN/search?_fields=email&email='+ email,
				type: 'GET'
			}).done(function(res) {
				if (res.length > 1) {
					alert("E-mail já cadastrado, por favor use um e-mail diferente");
					
					$('.form-newsletter').trigger("reset");					
				} else {
                    insertMasterData("CN", json_dados, function(res) {
                        console.log(res);
                        setTimeout(function() {
                            $('.radio-news').remove();
                            $('input[name="nome"]').remove();
                            $('input[name="email"]').remove();
                            $('input[type="submit"]').remove();
                            $('.form-newsletter').append('<div class="full msg_sucesso" style="color:white">Cadastro feito com sucesso!</div>');
                        }, 1000);
                        setTimeout(function() {
                            // $('.msg_sucesso').remove();
                        }, 10000);
                    });
				}
			});
		}, 1000);
    });
    
    $("p:contains('DESCRIÇÃO DO PRODUTO')").css('font-weight', 'bold').css('color', 'black');
    
    //MENU MOBILE
    $('.menu-mob .li-cat').append('<span></span>');
    
    $('.new-header-mob .menu-btn').on('click', function(){
        console.log('foi');
        $('.new-header-mob .overlay').toggleClass('active');
        $(this).toggleClass('active');
        $(this).find('span').toggleClass('active');
        $('.menu-mob').toggleClass('active');
    });
    
    
    $(document).on('click touchstart','.new-header-mob .overlay.active',  function(){
        $('.menu-mob').removeClass('active');
        $('.new-header-mob .menu-btn').removeClass('active');
        $('.new-header-mob .menu-btn span').removeClass('active');
        $(this).removeClass('active');
    });
    
    $('.new-header-mob .li-cat').on('click',function() {
        $(this).find('span').toggleClass('active');
        $(this).next('.sub-cat').slideToggle();
    });
    
    //PLUGIN INSTAGRAM
    if ($('#instafeed').length > 0) {
        var feed = new Instafeed({
            get: 'user',
            userId: 5483570827,
            accessToken: '5483570827.1677ed0.313cd120bddb4912b20c4d353254934a',
            target: 'instafeed',
            limit: '4',
            resolution: 'standard_resolution',
            template: '<div><a href="{{link}}" class="insta" target="_blank" ><img src="{{image}}" /><div class="likes">&hearts; {{likes}}</div></a></div>',
            after: function() {
                var el = $('#instafeed');
                if (el.classList)
                el.classList.add('show');
                else
                el.className += ' ' + 'show';
            }
        });
        
        feed.run();
    }
    
    //NEWSLETTER FOOTER
    $('.new-footer-mob .ft-newsletter .newsletter input[type="button"]').val('EU QUERO!');
    
    //GERAL
    $('.helperComplement').remove();
    $(".lancamentos-mobile ul").addClass("lancamentos");
    $(".destaques-mobile ul").addClass("destques");
    $(".thumbs").addClass("banner-mobile");
    $(".forms_parcelamento").click(function() {
        $(".hiddenParce").toggle("slow");
    });
    $('.mobile-open-filter').on('click', function(){
        $('.navigation-tabs').toggleClass('ativo');
        $('.navigation').toggleClass('ativo');
    });
    $(".search-multiple-navigator").prepend('<li class="appendFilter">FILTROS</li>');
    $(".search-single-navigator").prepend('<li class="appendFilter">FILTROS</li>');
    function slickThumb(argument) {
        setTimeout(function() {
            $(".thumbs img").addClass("persoThumbs");
            $(".thumbs").find('.persoThumbs')
            .each(function() {
                var src = $(this).attr('src');
                src = src.replace('-90-140/', '-1000-1000/');
                $(this).attr({
                    src: src,
                    width: '100%',
                    height: '100%'
                });
            });
            $('.banner-mobile').slick({
                adaptiveHeight: true,
                infinite: false,
                dots: true,
                autoplay: true,
                autoplaySpeed: 8000,
                mobileFirst: true,
            });
        }, 1000);
    }
    
    var trigger = $('.hamburger'),
    overlay = $('.overlay'),
    isClosed = false;
    
    function buttonSwitch() {
        
        if (isClosed === true) {
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }
    
    trigger.click(function() {
        buttonSwitch();
    });
    
    $('[data-toggle="offcanvas"]').click(function() {
        $('.menuPrincipal').toggleClass('toggled');
    });
    
    $('.subcat').hide();
    
    $('.catMenu').click(function(event) {
        event.stopPropagation();
        $(this).next('ul').slideToggle('slow');
    });
    
    $('.banner-mobile').slick({
        adaptiveHeight: true,
        infinite: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 8000,
        mobileFirst: true,
    });
    
    $('.lancamentos').slick({
        swipeToSlide: true,
        infinite: true,
        adaptiveHeight: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 8000,
        mobileFirst: true,
    });
    
    
    $('.skuList label').on('click', function(){
        $('.banner-mobile').slick('unslick');
        slickThumb();
        
    });
    
    if ($('.thumbs.banner-mobile').hasClass('slick-initialized')) {
        $('.thumbs.banner-mobile').removeClass('sem-slick');
        console.log('tem');
    } else {
        $('.thumbs.banner-mobile').addClass('sem-slick');
        console.log('nÃ£o tem');
    }
    
    slickThumb();
    
    $('.tipbar.full').slick({
        infinite: false,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 8000,
        slidesToShow: 1,
        slidesToScroll: 1,
        mobileFirst: true
    });
});

$(window).scroll(function(){
    var sticky = $(".menuFixed"),
    scroll = $(window).scrollTop();
    
    if (scroll >= 100) sticky.addClass("fixedMenu");
    else sticky.removeClass("fixedMenu");
});

$(window).on('load', function(){
    $('#instafeed h2').addClass('instafeed-title');
    $('#instafeed').before($('#instafeed h2'));
    $('#instafeed').slick({slidesToShow: 1,slidesToScroll: 1}).slick('slickFilter', 'div');
});

function formatReal(int) {
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if (tmp.length > 6)
    tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    
    return tmp;
}

function ameCashBack() {
    $('.ame_cashback').each(function(index, element) {
		var id_prat = $(this).data('id');

		vtexjs.catalog.getProductWithVariations(id_prat).done(function(product) {
			$.each(product.skus, function (index, sku) {
				if (sku.available != false) {
					var preco = sku.bestPrice;
					var percentage = (preco * 30) / 100;
					var percentage_number = (percentage * 100) / preco;
					var money_percent = formatReal(percentage);
					
					// console.log(preco);
					// console.log(percentage);
					// console.log(percentage_number);
					// console.log(money_percent);
					// console.log(element);
					
					$(element).find('span#cash_back').html("R$" + money_percent + "<br>");
                    
				}
			});
		});
	});
    
    if ($('body').hasClass('mobProduto')) {
		$.each(skuJson.skus, function (index, sku){
		    if (sku.available != false) {
				var preco = sku.bestPrice
				var percentage = (preco * 30) / 100;
				var percentage_number = (percentage * 100) / preco;
				var money_percent = formatReal(percentage);
			
				// console.log(preco);
				// console.log(percentage);
				// console.log(percentage_number);
				// console.log(money_percent);
				// console.log(element);
			
				$(".cashback_ame").css("display", "inline-flex");
				$('.cashback_ame .cash_back').html("R$" + money_percent);
		    }
		});
	}
}

function freteGratis() {
	$(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
		console.log(orderForm.totalizers.length === 1);
		console.log(orderForm.totalizers.length);
		if (orderForm.totalizers.length > 0) {
			var desconto2 = orderForm.totalizers[0].value;
			var finalPromo2 = 50000 - desconto2;
			var valor_porcento2 = (desconto2 / 50000) * 100;
			var teste2 = finalPromo2 / 100;
			var finalFeliz2 = teste2.toFixed(2);
			var finalFeliz2 = finalFeliz2.replace(".", ",");
			// var finalSplit2 = (desconto2 / 100);

			if (desconto2 < 50000) {

				$('.freteGratis').css({
					'background': '-moz-linear-gradient(90deg, rgba(245,124,34,1) 0%, rgba(253,192,104,1) '+ Math.round(valor_porcento2) +'%, rgba(0,0,0,1) '+ Math.round(valor_porcento2) +'%, rgba(80,80,80,1) 100%)',
					'background': '-webkit-linear-gradient(90deg, rgba(245,124,34,1) 0%, rgba(253,192,104,1) '+ Math.round(valor_porcento2) +'%, rgba(0,0,0,1) '+ Math.round(valor_porcento2) +'%, rgba(80,80,80,1) 100%)',
					'background': 'linear-gradient(90deg, rgba(245,124,34,1) 0%, rgba(253,192,104,1) '+ Math.round(valor_porcento2) +'%, rgba(0,0,0,1) '+ Math.round(valor_porcento2) +'%, rgba(80,80,80,1) 100%)'
				});
				$(".freteGratis .fraseFrete2").html('Com mais R$ ' + finalFeliz2 + ' <span>  o frete é por nossa conta!</span>');

			} else {
				$('.freteGratis').addClass('conseguiu');
				$(".freteGratis .fraseFrete2").html('Parabéns! Você ganhou o Frete Grátis!');
			}

		}

	});
}

function ajaxCallback() {
	console.log("teste");
	$('.search-multiple-navigator input[type="checkbox"]').vtexSmartResearch({
		shelfCallback: function () {
			console.log('shelfCallback');
		},

		ajaxCallback: function () {
			console.log('ajaxCallback');
            ameCashBack();
            carrinhoFlutante.btn_buy();
		}
	});
}

var carrinhoFlutante = {

	toggle_carrinho: function () {
		$('header .cart a.btnCart').on('click', function (event) {
			event.preventDefault();
			$('#cart-lateral, #overlay, body').toggleClass('active');
		});
	},

	calculateShipping: function () {
		if ($('#search-cep input[type="text"]').val() != '') {
			vtexjs.checkout.getOrderForm()
				.then(function (orderForm) {
					if (localStorage.getItem('cep') === null) {
						var postalCode = $('#search-cep input[type="text"]').val();
					} else {
						var postalCode = localStorage.getItem('cep');
					}

					var country = 'BRA';
					var address = {
						"postalCode": postalCode,
						"country": country
					};

					//EFEITO FADE
					$('#cart-lateral .footer ul li span.frete .box-1').fadeOut(300, function () {
						$('#cart-lateral .footer ul li span.frete .box-2').fadeIn(300);
					});

					return vtexjs.checkout.calculateShipping(address);
				})
				.done(function (orderForm) {
					if (orderForm.totalizers.length == 0) {
						$('#cart-lateral .value-frete').text('Você não tem itens no carrinho!');
					} else {
						var value_frete = orderForm.totalizers[1].value / 100;
						value_frete = value_frete.toFixed(2).replace('.', ',').toString();
						$('#cart-lateral .value-frete').text('R$: ' + value_frete);

						var postalCode = $('#search-cep input[type="text"]').val();
						localStorage.setItem('cep', postalCode);
						$('#search-cep input[type="text"]').val(postalCode);
					}
				});
		}
	},

	//APOS INSERIDO - CALCULA FRETE AO CARREGAR A PG
	automaticCalculateShipping: function () {
		$(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
			if (localStorage.getItem('cep') != null) {
				$('#search-cep input[type="text"]').val(localStorage.getItem('cep'));
				carrinhoFlutante.calculateShipping();
			}
		});
	},

	fadeAction: function () {
		$('#cart-lateral .footer ul li span.frete a').on('click', function (e) {
			e.preventDefault();
			$('#cart-lateral .footer ul li span.frete a').fadeOut(300, function () {
				$('#cart-lateral .footer ul li span.frete .box-1 div').fadeIn(300);
			});
		});

		//CALCULAR NOVAMENTE
		$('#cart-lateral .return-frete').on('click', function () {
			$('#cart-lateral .footer ul li span.frete .box-2').fadeOut(300, function () {
				$('#cart-lateral .footer ul li span.frete .box-1').fadeIn(300);
			});
		});
	},

	//CALCULA MANUALMENTE
	calculaFrete: function () {
		//MASK
		// $('#search-cep input[type="text"]').mask('00000-000');

		//CLICK
		$('#search-cep input[type=submit]').on('click', function (e) {
			e.preventDefault();
			if ($('#search-cep input[type="text"]').val().length === 9) {
				carrinhoFlutante.calculateShipping();
				$('#search-cep input[type="text"]').removeClass('active');
			} else {
				$('#search-cep input[type="text"]').addClass('active');
			}
		});

		//PRESS ENTER
		$('#search-cep input[type=text]').on('keypress', function (event) {
			if (keycode == '13') {
				if ($('#search-cep input[type="text"]').val().length === 9) {
					var keycode = (event.keyCode ? event.keyCode : event.which);
					carrinhoFlutante.calculateShipping();
					$('#search-cep input[type="text"]').removeClass('active');
				} else {
					$('#search-cep input[type="text"]').addClass('active');
				}
			}
		});
	},

	cartLateral: function () {
		vtexjs.checkout.getOrderForm()
			.done(function (orderForm) {
				//TOTAL CARRINHO
				var quantidade = 0;
				for (var i = orderForm.items.length - 1; i >= 0; i--) {
					quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
				}

				$('header .cart span').text(quantidade);

				//INFORMACOES DO CARRINHO
				if (orderForm.value != 0) {
					total_price = orderForm.value / 100;
					total_price = total_price.toFixed(2).replace('.', ',').toString();

					$('#cart-lateral .footer .total-price').text('R$: ' + total_price);
				} else {
					$('#cart-lateral .footer .total-price').text('R$: 0,00');
				}

				if (orderForm.totalizers.length != 0) {
					sub_price = orderForm.totalizers[0].value / 100;
					sub_price = sub_price.toFixed(2).replace('.', ',').toString();

					$('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: ' + sub_price);
				} else {
					$('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: 0,00');
				}

				if (orderForm.items != 0) {
					total_items = orderForm.items.length;

					$('#cart-lateral .header .total-items span').text(total_items + ' Itens');
				} else {
					$('#cart-lateral .header .total-items span').text('0 Itens');
				}
				//FIM - INFORMACOES DO CARRINHO

				//ITEMS DO CARRINHO
				$('#cart-lateral .content ul li').remove();
				for (i = 0; i < orderForm.items.length; i++) {

					price_item = orderForm.items[i].price / 100;
					price_item = price_item.toFixed(2).replace('.', ',').toString();
					id_prod = orderForm.items[i].id;
					

					var content = '';

					content += '<li class="cartItem" data-index="' + i + '" data-id="'+ id_prod +'">';
					content += '<div class="column_1"><img src="' + orderForm.items[i].imageUrl + '" alt="' + orderForm.items[i].name + '"/></div>';

					content += '<div class="column_2">';
					content += '<div class="name">';
					content += '<p>' + orderForm.items[i].name + '</p>';
					content += '</div>';

					content += '<div class="ft">';
					content += '<ul>';

					content += '<li class="price">';
					content += '<p>R$: ' + price_item + '</p>';
					content += '</li>';

					content += '<li data-index="' + i + '">';
					content += '<div class="box-count">';
					content += '<a href="" class="count count-down">-</a>';
					content += '<input type="text" value="' + orderForm.items[i].quantity + '" />';
					content += '<a href="" class="count count-up">+</a>';
					content += '</div>';
					content += '</li>';

					content += '<ul>';

					content += '</div>';
					content += '</div>';

					content += '<span class="removeUni">x</span>';
					content += '</li>';



					$('#cart-lateral .content > ul').append(content);
				}
				//FIM - ITEMS DO CARRINHO

				freteGratis();
			});
	},

	changeQuantity: function () {
		$(document).on('click', '#cart-lateral .columns>.column_2 .content ul li .column_2 .ft ul li .box-count .count', function (e) {
			e.preventDefault();

			var qtd = $(this).siblings('input[type="text"]').val();
			if ($(this).hasClass('count-up')) {
				qtd++
				$(this).siblings('input[type="text"]').removeClass('active');
				$(this).siblings('input[type="text"]').val(qtd);
			} else if ($(this).hasClass('count-down')) {
				if ($(this).siblings('input[type="text"]').val() != 1) {
					qtd--
					$(this).siblings('input[type="text"]').val(qtd);
				} else {
					//ALERTA 0 USUARIO QUANTIDADE NEGATIVA
					$(this).siblings('input[type="text"]').addClass('active');
				}
			}

			var data_index = $(this).parents('li').data('index');
			var data_quantity = $(this).parents('li').find('.box-count input[type="text"]').val();

			vtexjs.checkout.getOrderForm()
				.then(function (orderForm) {
					var total_produtos = parseInt(orderForm.items.length);
					vtexjs.checkout.getOrderForm()
						.then(function (orderForm) {
							var itemIndex = data_index;
							var item = orderForm.items[itemIndex];

							var updateItem = {
								index: data_index,
								quantity: data_quantity
							};

							return vtexjs.checkout.updateItems([updateItem], null, false);
						})
						.done(function (orderForm) {
							carrinhoFlutante.cartLateral();
						});
				});
		});
	},

	//OPEN QUANTITY
	openQuantity: function () {
		$(document).on('click', '#cart-lateral .ft .btn', function () {
			$(this).toggleClass('active');
			$(this).next('ul').slideToggle();
		});
	},

	removeItems: function () {
		$(document).on('click', '#cart-lateral .columns > .column_2 .content ul li .removeUni', function () {

			var data_index = $(this).parents('li').data('index');
			var data_quantity = $(this).siblings('li').find('.box-count input[type="text"]').val();

			vtexjs.checkout.getOrderForm()
				.then(function (orderForm) {
					var itemIndex = data_index;
					var item = orderForm.items[itemIndex];
					var itemsToRemove = [{
						"index": data_index,
						"quantity": data_quantity,
					}]
					return vtexjs.checkout.removeItems(itemsToRemove);
				})
				.done(function (orderForm) {
					carrinhoFlutante.cartLateral();
					setTimeout(() => {
						
						if ($('#cart-lateral .columns>.column_2 .content ul li').length < 1) {
							$('.freteGratis').addClass('semFrete');
							$('.freteGratis').removeAttr('style');
							$('.freteGratis').removeClass('conseguiu');
							$(".freteGratis .fraseFrete2").html('Seu carrinho está vazio, acumulando R$ 500,00 reais em compra, <span>o frete será gratuito!</span>');
						} else{
							freteGratis();
						}

					}, 2000);
				});
				
		});
	},

	removeAllItems: function () {
		$('#cart-lateral .clear').on('click', function () {
			vtexjs.checkout.removeAllItems()
				.done(function (orderForm) {
					//ATUALIZA O CARRINHO APÓS ESVAZIAR
					carrinhoFlutante.cartLateral();
				});
		});
	},

	add_to_cart: function () {
		$(document).on('click', '.prateleira-basica .sprite-comprar-async', function () {

			$(this).addClass('loading');

			var prodSku = $(this).parents('.comprar-produto').find('.sku.estoque-true.ativo').data('sku');
			

			vtexjs.catalog.getProductWithVariations($(this).parents('article').data('id')).done(function (product) {
				console.log(product);

				var item = {
					id: prodSku,
					quantity: 1,
					seller: '1'
				};

				vtexjs.checkout.getOrderForm()
				vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {

					$('.prateleira .prateleira-cta').removeClass('loading');

					carrinhoFlutante.cartLateral();

					//ATIVA CARRINHO LATERAL
					$('header .cart a.btnCart').trigger('click');
				});
			});

		});
	},

	btn_buy: function () {
		window.alert = function () {
			$('header .cart a.btnCart').trigger('click');
			carrinhoFlutante.cartLateral();
		}
	},

	openCart: function () {
		// $('#cart-lateral .header').on('click', function () {
		// 	$('#cart-lateral, #overlay').toggleClass('active');
		// });

		$('#overlay, #cart-lateral .header .close').on('click', function () {
			if ($('#cart-lateral').hasClass('active')) {
				
				$('#cart-lateral, #overlay, body').removeClass('active');
			}
		});
	}
}

$(document).ready(function() {

	freteGratis();

	carrinhoFlutante.toggle_carrinho();

	//FRETE
	// carrinhoFlutante.calculateShipping();
	// carrinhoFlutante.automaticCalculateShipping();
	// carrinhoFlutante.fadeAction();
	// carrinhoFlutante.calculaFrete();
	//FIM - FRETE

	carrinhoFlutante.cartLateral();
	carrinhoFlutante.changeQuantity();
	carrinhoFlutante.openQuantity();
	carrinhoFlutante.removeItems();
	carrinhoFlutante.removeAllItems();
	carrinhoFlutante.btn_buy();
	carrinhoFlutante.openCart();
    
    if ($('body').hasClass('departamento')){
    	ajaxCallback();
	}
    
    $('.ame_cashback').each(function(index, element) {
		var id_prat = $(this).data('id');

		vtexjs.catalog.getProductWithVariations(id_prat).done(function(product) {
			$.each(product.skus, function (index, sku) {
				if (sku.available != false) {
					var preco = sku.bestPrice;
					var percentage = (preco * 30) / 100;
					var percentage_number = (percentage * 100) / preco;
					var money_percent = formatReal(percentage);
					
					// console.log(preco);
					// console.log(percentage);
					// console.log(percentage_number);
					// console.log(money_percent);
					// console.log(element);
					
					$(element).find('span#cash_back').html("R$" + money_percent + "<br>");
                    
				}
			});
		});
	});
    
    if ($('body').hasClass('mobProduto')) {
		$.each(skuJson.skus, function (index, sku){
		    if (sku.available != false) {
				var preco = sku.bestPrice
				var percentage = (preco * 30) / 100;
				var percentage_number = (percentage * 100) / preco;
				var money_percent = formatReal(percentage);
			
				// console.log(preco);
				// console.log(percentage);
				// console.log(percentage_number);
				// console.log(money_percent);
				// console.log(element);
			
				$(".cashback_ame").css("display", "inline-flex");
				$('.cashback_ame .cash_back').html("R$" + money_percent);
		    }
		});
	}
    
    if ($('body.departamento').length) {
        
        // $(".menu-departamento input[type='checkbox']").vtexSmartResearch();
        
    }
    
    $('.btn_mobile .seeAll_lancamentos').attr('href', 'https://store.pierrecardin.com.br/masculino?PS=12&O=OrderByReleaseDateDESC');
    $('.btn_mobile .seeAll_destaques').attr('href', 'https://store.pierrecardin.com.br/masculino/147?PS=12&map=c,productClusterIds&O=OrderByTopSaleDESC');
});
